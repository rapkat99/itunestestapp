//
//  AlbumsDetailModel.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation

struct AlbumsDetailModel: Codable {
    var results: [Songs]
}

struct Songs: Codable {
    let wrapperType: String?
    let trackName: String?
    let artworkUrl100: String?
    let artistName: String?
    let collectionName: String?
}
