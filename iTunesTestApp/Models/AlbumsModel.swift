//
//  AlbumsModel.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation

struct AlbumsModel: Codable {
    let results: [albumsResult]
}

struct albumsResult: Codable {
    let collectionId: Int
    let artistName: String
    let collectionName: String
    let artworkUrl100: String
}
