//
//  Server.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation
import Alamofire

protocol ServerType {
    func getAlbums(searchText: String, completion: @escaping (Result<AlbumsModel, Error>) -> Void)
    func getAlbumDetail(albumId: String, completion: @escaping (Result<AlbumsDetailModel, Error>) -> Void)
}

class Server: ServerType {
    let baseURl = "https://itunes.apple.com/"
    let decoder: JSONDecoder
    
    init(decoder: JSONDecoder) {
        self.decoder = decoder
    }
    
    func handleResponse<T: Decodable>(model: T.Type, response: DataResponse<T, AFError>, completion: @escaping (Result<T, Error>) -> Void) {
        switch response.result {
        case .success(let t):
            completion(.success(t))
        case .failure(let err):
            completion(.failure(err))
        }
    }
    
    func getAlbums(searchText: String, completion: @escaping (Result<AlbumsModel, Error>) -> Void) {
        let params: [String: String] = ["term": searchText ,
                                        "entity": "album"]
        AF.request(baseURl.appending("search"),
                   method: .get,
                   parameters: params)
            .validate()
            .responseDecodable(of: AlbumsModel.self, decoder: decoder) { (response) in
                self.handleResponse(model: AlbumsModel.self, response: response, completion: completion)
            }
    }
    
    func getAlbumDetail(albumId: String, completion: @escaping (Result<AlbumsDetailModel, Error>) -> Void) {
        let params: [String: String] = ["id": albumId,
                                        "entity": "song"]
        AF.request(baseURl.appending("lookup"),
                   method: .get,
                   parameters: params)
            .validate()
            .responseDecodable(of: AlbumsDetailModel.self, decoder: decoder) { (response) in
                self.handleResponse(model: AlbumsDetailModel.self, response: response, completion: completion)
            }
    }
}
