//
//  RequestManager.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation
import Alamofire

class RequestManager {

    typealias Completion = (_ response: Any?, _ error: Error?, _ isSuccess: Bool) -> Void

    static func request(with url: URL, method: HTTPMethod,
                        params: [String: Any]?, header: HTTPHeaders?,
                        completion: @escaping Completion) {
        AF.request(url, method: method, parameters: params,
                   encoding: JSONEncoding.default,
                   headers: header)
            .responseJSON { response in
                    switch response.result {
                    case .success:
                        if response.response?.statusCode ?? 0 < 220 {
                            completion(response, nil, true)
                        } else {
                            completion(response, nil, false)
                        }
                    case .failure(let error):
                        completion(nil, error, false)
            }
        }
    }
}
