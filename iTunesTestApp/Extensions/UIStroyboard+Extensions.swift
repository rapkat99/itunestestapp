//
//  UIStroyboard+Extensions.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation
import UIKit

extension UIStoryboard {
    class func createVC<T: UIViewController>(controllerType: T.Type) -> T {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = (storyboard.instantiateViewController(withIdentifier: controllerType.className) as? T)!
        return viewController
    }
}

extension NSObject {
    static var className: String {
        return String(describing: self)
    }
}
