//
//  ServerAssembly.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

class ServerAssembly: Assembly {
    func assemble(container: Container) {
        container
            .autoregister(ServerType.self, initializer: Server.init)
            .inObjectScope(.container)
        
        container
            .register(JSONDecoder.self) { (r) -> JSONDecoder in
                return JSONDecoder()
            }
            .inObjectScope(.container)
    }
}

class AlbumAssembly: Assembly {
    func assemble(container: Container) {
        container
            .autoregister(AlbumsViewModelType.self, initializer: AlbumsViewModel.init)
            .inObjectScope(.transient)
        
        container.storyboardInitCompleted(AlbumsViewController.self) { (r, vc) in
            vc.viewModel = r~>
        }
        
        container
            .autoregister(AlbumDetailViewModelType.self, initializer: AlbumDetailViewModel.init)
            .inObjectScope(.transient)
        
        container.storyboardInitCompleted(AlbumDetailViewController.self) { (r, vc) in
            vc.viewModel = r~>
        }
    }
}
