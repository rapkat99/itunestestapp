//
//  AlbumCell.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 9/3/22.
//

import UIKit
import Kingfisher

class AlbumCell: UICollectionViewCell {
    
    static let identifier = "AlbumCell"
        
    static func nib() -> UINib {
        return UINib(nibName: "AlbumCell", bundle: nil)
    }

    @IBOutlet private weak var albumImage: UIImageView!
    @IBOutlet private weak var albumNameLabel: UILabel!
    @IBOutlet private weak var albumAuthorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(albumData: albumsResult?) {
        albumNameLabel.text = albumData?.collectionName
        albumAuthorLabel.text = albumData?.artistName
        let url = URL(string: albumData?.artworkUrl100 ?? "")
        albumImage.kf.setImage(with: url)
    }
}
