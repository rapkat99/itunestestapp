//
//  AlbumsViewModel.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation
protocol AlbumsViewModelType {
    func getAlbums(searchText: String, completion: @escaping (Result<AlbumsModel, Error>) -> Void)
}

class AlbumsViewModel: AlbumsViewModelType {
    private let server: ServerType
    
    init(server: ServerType) {
        self.server = server
    }
    
    func getAlbums(searchText: String, completion: @escaping (Result<AlbumsModel, Error>) -> Void) {
        server.getAlbums(searchText: searchText, completion: completion)
    }
}
