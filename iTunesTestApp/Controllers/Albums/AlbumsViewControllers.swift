//
//  AlbumsViewControllers.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 9/3/22.
//

import UIKit

class AlbumsViewController: UIViewController {
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var albums: AlbumsModel?
    
    var viewModel: AlbumsViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setupSearchBar()
        setupCollectionView()
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(AlbumCell.nib(), forCellWithReuseIdentifier: AlbumCell.identifier)
    }
    
    private func getAlbums(searchText: String) {
        viewModel.getAlbums(searchText: searchText) { [weak self] repsonse in
            guard let self = self else { return }
            switch repsonse {
            case .success(let albums):
                self.albums = albums
                self.collectionView.reloadData()
            case .failure(let err):
                print(err)
            }
        }
    }
}

extension AlbumsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        albums?.results.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCell.identifier, for: indexPath) as? AlbumCell else {
            return UICollectionViewCell()
        }
        cell.setupCell(albumData: albums?.results[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let albumDetailVC = UIStoryboard.createVC(controllerType: AlbumDetailViewController.self)
        albumDetailVC.viewModel.albumId = String(albums?.results[indexPath.row].collectionId ?? .zero)
        navigationController?.pushViewController(albumDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: 64)
    }
}

extension AlbumsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getAlbums(searchText: searchBar.text ?? "")
    }
}
