//
//  AlbumDetailViewController.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import UIKit
import Kingfisher

class AlbumDetailViewController: UIViewController {
    
    @IBOutlet private weak var albumImage: UIImageView!
    @IBOutlet private weak var albumNameLabel: UILabel!
    @IBOutlet private weak var albumAuthorLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: AlbumDetailViewModelType!
    private var albumDetailModel: AlbumsDetailModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getAlbumDetailById()
    }
    
    private func setupView() {
        title = "Album Detail"
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func getAlbumDetailById() {
        viewModel.getAlbumDetail() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let albumDetailModel):
                self.albumDetailModel = albumDetailModel
                self.setupAlbumSongs(albumSongs: albumDetailModel)
                self.setupUI(songDetail: albumDetailModel.results.last)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func setupAlbumSongs(albumSongs: AlbumsDetailModel) {
        for i in 0..<albumSongs.results.count {
            if albumSongs.results[i].wrapperType != "track" {
                self.albumDetailModel?.results.remove(at: i)
            }
        }
        self.tableView.reloadData()
    }
    
    private func setupUI(songDetail: Songs?) {
        let url = URL(string: songDetail?.artworkUrl100 ?? "")
        self.albumImage.kf.setImage(with: url)
        self.albumNameLabel.text = songDetail?.collectionName ?? ""
        self.albumAuthorLabel.text = songDetail?.artistName ?? ""
    }
}

extension AlbumDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumDetailModel?.results.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = (albumDetailModel?.results[indexPath.row].trackName ?? "")
        return cell
    }
}
