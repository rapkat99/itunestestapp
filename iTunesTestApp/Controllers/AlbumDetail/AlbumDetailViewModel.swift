//
//  AlbumDetailViewModel.swift
//  iTunesTestApp
//
//  Created by Baudunov Rapkat on 10/3/22.
//

import Foundation

protocol AlbumDetailViewModelType {
    var albumId: String? { get set }
    func getAlbumDetail(completion: @escaping (Result<AlbumsDetailModel, Error>) -> Void)
}

class AlbumDetailViewModel: AlbumDetailViewModelType {
    var albumId: String?
    
    private let server: ServerType
    
    init(server: ServerType) {
        self.server = server
    }
    
    func getAlbumDetail(completion: @escaping (Result<AlbumsDetailModel, Error>) -> Void) {
        server.getAlbumDetail(albumId: self.albumId ?? "", completion: completion)
    }
}
